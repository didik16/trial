<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@test.id',
            'password' => bcrypt('12345678'),
        ]);

        $admin->assignRole('admin');

        $finance = User::create([
            'name' => 'Finance',
            'email' => 'finance@test.id',
            'password' => bcrypt('12345678'),
        ]);

        $finance->assignRole('finance');


        $supervisor = User::create([
            'name' => 'Supervisor',
            'email' => 'supervisor@test.id',
            'password' => bcrypt('12345678'),
        ]);

        $supervisor->assignRole('supervisor');


        $editor = User::create([
            'name' => 'Editor',
            'email' => 'editor@test.id',
            'password' => bcrypt('12345678'),
        ]);

        $editor->assignRole('editor');

        $user = User::create([
            'name' => 'User',
            'email' => 'user@test.id',
            'password' => bcrypt('12345678'),
        ]);

        $user->assignRole('user');

        $user2 = User::create([
            'name' => 'User',
            'email' => 'user2@test.id',
            'password' => bcrypt('12345678'),
        ]);

        $user2->assignRole('user');
    }
}
