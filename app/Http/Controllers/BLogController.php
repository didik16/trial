<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Auth;
use App\Blog;


class BLogController extends Controller
{

    protected $request;


    public function index()
    {

        $data = Blog::orderBy('created_at', 'DESC')->get();

        return view('editor/blog/index', [
            'data' => $data
        ]);
    }

    public function add()
    {
        return view('editor/blog/add');
    }


    public function store(Request $req)
    {


        $messages = [
            'title.required' => 'Mohon isi form judul',
        ];
        $this->validate($req, [
            'title' => 'required',
        ], $messages);


        if ($req->hasFile('photo')) {
            $resorce = $req->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/image", $name);
        }

        Blog::create([
            'title' => $req->title,
            'category' =>  $req->category,
            'body' => $req->content,
            'image' => $name,
        ]);

        return redirect()->back();
    }

    public function edit($id)
    {
        $data = Blog::where('id', $id)->firstOrFail();

        return view('editor/blog/edit', ['data' => $data]);
    }

    public function update($id, Request $request)
    {
        $messages = [
            'title.required' => 'Mohon isi form judul',
        ];
        $this->validate($request, [
            'title' => 'required',
        ], $messages);

        if ($request->hasFile('photo')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/image", $name);

            $blog = Blog::find($id);
            $blog->title = $request->title;
            $blog->category = $request->category;
            $blog->body = $request->content;
            $blog->image = $request->name;
            $blog->save();
        } else {
            $blog = Blog::find($id);
            $blog->title = $request->title;
            $blog->category = $request->category;
            $blog->body = $request->content;
            $blog->save();
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        $delete = Blog::where('id', $id)->delete();

        return redirect()->back();
    }
}
