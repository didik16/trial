<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Auth;
use App\Income;


class IncomeController extends Controller
{

    protected $request;


    public function index()
    {

        $data = Income::orderBy('created_at', 'DESC')->get();

        return view('accounting/income/index', [
            'data' => $data
        ]);
    }

    public function add()
    {

        return view('accounting/income/add_income');
    }

    public function store(Request $req)
    {

        $messages = [
            // 'total_harga.required' => 'Please Fill the Correct Data',
        ];

        $this->validate($req, [
            'nama_barang' => 'required',
            'harga_barang' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'photo' => 'required',
            'total' => 'required|numeric',
            'nama_pembeli' => 'required',
            'alamat_pembeli' => 'required',
        ], $messages);

        if ($req->hasFile('photo')) {
            $resorce = $req->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/image", $name);
        }

        Income::create([
            'nama_barang' => $req->nama_barang,
            'harga_barang' =>  $req->harga_barang,
            'jumlah' => $req->jumlah,
            'gambar' => $name,
            'total' => $req->total,
            'nama_pembeli' => $req->nama_pembeli,
            'alamat_pembeli' => $req->alamat_pembeli,
        ]);

        return redirect()->back();
    }

    public function edit($id)
    {
        $data = Income::where('id', $id)->firstOrFail();

        return view('accounting/income/edit', ['data' => $data]);
    }


    public function update($id, Request $request)
    {
        $messages = [
            // 'total_harga.required' => 'Please Fill the Correct Data',
        ];

        $this->validate($request, [
            'nama_barang' => 'required',
            'harga_barang' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'total' => 'required|numeric',
            'nama_pembeli' => 'required',
            'alamat_pembeli' => 'required',
        ], $messages);

        if ($request->hasFile('photo')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/image", $name);

            $blog = Income::find($id);
            $blog->nama_barang = $request->nama_barang;
            $blog->harga_barang = $request->harga_barang;
            $blog->jumlah = $request->jumlah;
            $blog->gambar = $request->name;
            $blog->total = $request->total;
            $blog->nama_pembeli = $request->nama_pembeli;
            $blog->alamat_pembeli = $request->alamat_pembeli;
            $blog->save();
        } else {
            $blog = Income::find($id);
            $blog->nama_barang = $request->nama_barang;
            $blog->harga_barang = $request->harga_barang;
            $blog->jumlah = $request->jumlah;
            $blog->total = $request->total;
            $blog->nama_pembeli = $request->nama_pembeli;
            $blog->alamat_pembeli = $request->alamat_pembeli;
            $blog->save();
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        $delete = Income::where('id', $id)->delete();

        return redirect()->back();
    }
}
