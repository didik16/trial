@extends('app.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Internal User</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Internal User</a></div>
                    <div class="breadcrumb-item"><a href="#">Forms</a></div>
                    <div class="breadcrumb-item">Editor</div>
                </div>
            </div>

            <a href="{{ route('income.add') }}" class="btn btn-primary mb-3" style="color: white">Add Data</a>

            @php $no=1;@endphp
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Dibuat</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Dibuat</th>
                    </tr>
                </tfoot>
                @foreach ($data as $p)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $p->name }}</td>
                        <td>{{ $p->email }}</td>
                        <td>{{ $p->nama_role }}</td>
                        <td>{{ $p->created_at }}</td>
                        <td>
                            <a href="/accounting/income/edit/{{ $p->id }}" class="btn btn-primary"
                                style="color: white">Edit</a>
                            <a href="/accounting/income/delete/{{ $p->id }}" class="btn btn-danger"
                                style="color: white">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>

        </section>
    </div>

@endsection
