<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Detail_user;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class MyProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // $data = User::where('id', Auth::user()->id)->first();
        $data = User::with(['detail_user', 'detail_user.user'])->firstOrFail();

        return view('my_profile/index', ['data' => $data]);
    }

    public function update(Request $request)
    {

        $id =  Auth::user()->id;
        $messages = [];
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',

        ], $messages);

        if ($request->hasFile('photo')) {
            $resorce = $request->file('photo');
            $name_photo = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/image", $name_photo);

            $user = User::find($id);
            $user->name = $request->name_photo;
            $user->email = $request->email;
            $user->save();

            $user_detail = Detail_user::where('user_id', $id)->first();

            if (!empty($user_detail)) {
                $user_detail->phone = $request->phone;
                $user_detail->address = $request->address;
                $user_detail->image = $name_photo;
                $user_detail->save();
            } else {

                Detail_user::create([
                    'user_id' => $id,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'image' => $name_photo,
                ]);
            }
        } else {

            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            $user_detail = Detail_user::where('user_id', $id)->first();

            if (!empty($user_detail)) {
                $user_detail->phone = $request->phone;
                $user_detail->address = $request->address;
                $user_detail->save();
            } else {
                Detail_user::create([
                    'user_id' => $id,
                    'phone' => $request->phone,
                    'address' => $request->address,
                ]);
            }
        }

        return redirect()->back();
    }


    public function change_pass()
    {

        return view('my_profile/change_password');
    }

    public function update_pass()
    {
        // custom validator
        Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });

        // message for custom validation
        $messages = [
            'password' => 'Invalid current password.',
        ];

        // validate form
        $validator = Validator::make(request()->all(), [
            'current_password'      => 'required|password',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required',

        ], $messages);

        // if validation fails
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->errors());
        }

        // update password
        $user = User::find(Auth::id());

        $user->password = bcrypt(request('password'));
        $user->save();

        return redirect()
            ->route('my_profile')
            ->withSuccess('Password has been updated.');
    }
}
