<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['middleware' => ['role:admin']], function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('/user/public_user', 'UserController@public_user')->name('public_user');
        Route::get('/user/internal_user', 'UserController@internal_user')->name('internal_user');
        Route::get('/user/add_user', 'UserController@add_user')->name('add_user');
        Route::post('/user/store_add_user', 'UserController@store_add_user')->name('store_add_user');
    });

    Route::group(['middleware' => ['role:editor|admin']], function () {
        Route::get('/blog', 'BLogController@index')->name('blog');
        Route::get('/blog/add', 'BLogController@add')->name('blog.add');;
        Route::post('/blog/store', 'BLogController@store')->name('blog.store');
        Route::get('/blog/edit/{id}', 'BLogController@edit')->name('blog.edit');
        Route::post('/blog/update/{id}', 'BLogController@update')->name('blog.update');
        Route::get('/blog/delete/{id}', 'BLogController@delete')->name('blog.delete');
    });

    Route::group(['middleware' => ['role:finance|admin']], function () {
        Route::get('/accounting/income', 'IncomeController@index')->name('income');
        Route::get('/accounting/income/add', 'IncomeController@add')->name('income.add');
        Route::post('/accounting/income/store', 'IncomeController@store')->name('income.store');
        Route::get('/accounting/income/edit/{id}', 'IncomeController@edit')->name('income.edit');
        Route::post('/accounting/income/update/{id}', 'IncomeController@update')->name('income.update');
        Route::get('/accounting/income/delete/{id}', 'IncomeController@delete')->name('income.delete');

        Route::get('/accounting/expense', 'ExpenseController@index')->name('expense');
        Route::get('/accounting/expense/add', 'ExpenseController@add')->name('expense.add');
        Route::post('/accounting/expense/store', 'ExpenseController@store')->name('expense.store');
        Route::get('/accounting/expense/edit/{id}', 'ExpenseController@edit')->name('expense.edit');
        Route::post('/accounting/expense/update/{id}', 'ExpenseController@update')->name('expense.update');
        Route::get('/accounting/expense/delete/{id}', 'ExpenseController@delete')->name('expense.delete');
    });

    Route::get('/my_profile', 'MyProfileController@index')->name('my_profile');
    Route::post('/update_profile', 'MyProfileController@update')->name('update_profile');
    Route::get('/change_password', 'MyProfileController@change_pass')->name('change_pass');
    Route::post('/update_password', 'MyProfileController@update_pass')->name('update_pass');
});
