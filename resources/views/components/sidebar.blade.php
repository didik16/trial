<div class="navbar-bg"></div>

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">Stisla</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">

            @role('editor')
            <li class="menu-header">Blog</li>
            <li class="{{ Route::is('blog') ? 'active' : '' }}">
                <a class="nav-link " href="/blog"><i class="far fa-square"></i>
                    <span>Blog</span>
                </a>
            </li>
            @endrole

            @role('finance')
            <li class="menu-header">Accountant</li>
            <li class="{{ Route::is('income') ? 'active' : '' }} {{ Route::is('income.add') ? 'active' : '' }}"><a
                    class="nav-link" href="{{ route('income') }}"><i class="far fa-square"></i> <span>Income</span></a>
            </li>
            <li><a class="nav-link" href="{{ route('expense') }}"><i class="far fa-square"></i> <span>Expense</span></a>
            </li>
            @endrole

            @role('admin')
            <li class="menu-header">Dashboard</li>
            <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Blog</li>
            <li class="{{ Route::is('blog') ? 'active' : '' }}">
                <a class="nav-link " href="/blog"><i class="far fa-square"></i>
                    <span>Blog</span>
                </a>
            </li>
            <li class="menu-header">Accountant</li>
            <li class="{{ Route::is('income.add') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('income') }}"><i class="far fa-square"></i> <span>Income</span></a></li>
            <li><a class="nav-link" href="{{ route('expense') }}"><i class="far fa-square"></i> <span>Expense</span></a>
            </li>
            <li class="menu-header">User</li>
            <li><a class="nav-link" href="{{ route('internal_user') }}"><i class="far fa-square"></i> <span>Internal
                        User</span></a></li>
            <li><a class="nav-link" href="{{ route('public_user') }}"><i class="far fa-square"></i> <span>Public
                        User</span></a></li>
            @endrole
            <li class="menu-header">My Profile</li>
            <li><a class="nav-link" href="{{ route('my_profile') }}"><i class="far fa-square"></i>
                    <span>Profile</span></a></li>

        </ul>
    </aside>
</div>
