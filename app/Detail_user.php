<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_user extends Model
{
    protected $fillable = [
        'phone',
        'address',
        'user_id',
        'image',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
