<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $table = 'Incomes';

    protected $fillable = [
        'nama_barang',
        'harga_barang',
        'jumlah',
        'gambar',
        'total',
        'nama_pembeli',
        'alamat_pembeli',
    ];
}
