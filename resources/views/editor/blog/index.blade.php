@extends('app.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Blog</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Forms</a></div>
                    <div class="breadcrumb-item">Editor</div>
                </div>
            </div>

            <a href="{{ route('blog.add') }}" class="btn btn-primary mb-3" style="color: white">Add Data</a>

            @php $no=1;@endphp
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                @foreach ($data as $p)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $p->title }}</td>
                        <td>{{ $p->category }}</td>
                        <td>
                            <a href="/blog/edit/{{ $p->id }}" class="btn btn-primary" style="color: white">Edit</a>
                            <a href="/blog/delete/{{ $p->id }}" class=" btn btn-danger" style="color: white">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>

        </section>
    </div>

@endsection
