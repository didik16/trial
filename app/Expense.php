<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table = 'Expenses';

    protected $fillable = [
        'nama_barang',
        'harga_barang',
        'jumlah',
        'gambar',
        'total',
        'nama_suplier',
        'alamat_suplier',
    ];
}
