@extends('app.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Expense</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Forms</a></div>
                    <div class="breadcrumb-item">Editor</div>
                </div>
            </div>

            <a href="{{ route('expense.add') }}" class="btn btn-primary mb-3" style="color: white">Add Data</a>

            @php $no=1;@endphp
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Supplier</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Supplier</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                @foreach ($data as $p)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $p->nama_barang }}</td>
                        <td>Rp. {{ number_format($p->harga_barang) }}</td>
                        <td>{{ $p->jumlah }}</td>
                        <td>Rp. {{ number_format($p->total) }}</td>
                        <td>{{ $p->nama_suplier }}</td>
                        <td>
                            <a href="/accounting/expense/edit/{{ $p->id }}" class="btn btn-primary"
                                style="color: white">Edit</a>
                            <a href="/accounting/expense/delete/{{ $p->id }}" class="btn btn-danger"
                                style="color: white">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>

        </section>
    </div>

@endsection
