@extends('app.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Expense</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Forms</a></div>
                    <div class="breadcrumb-item">Editor</div>
                </div>
            </div>

            <div class="section-body">
                <h2 class="section-title">Expense</h2>

                <form action="/accounting/expense/update/{{ $data->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Edit Expense</h4>
                                </div>
                                <div class="card-body">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                                            Nama Barang</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="nama_barang" class="form-control"
                                                value="{{ $data->nama_barang }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Harga</label>
                                        <div class="col-sm-12 col-md-6">
                                            <input type="text" name="harga_barang" id="harga" class="form-control"
                                                value="{{ $data->harga_barang }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jumlah</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="jumlah" id="jumlah" class="form-control"
                                                value="{{ $data->jumlah }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Total
                                            Harga</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="total" id="total" class="form-control"
                                                value="{{ $data->total }}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Foto</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="photo" class="dropify">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama
                                            Suplier</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="nama_suplier" class="form-control"
                                                value="{{ $data->nama_suplier }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat
                                            Suplier</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="alamat_suplier" class="form-control"
                                                value="{{ $data->alamat_suplier }}">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>

@section('script')
    <script type="text/javascript">
        window.onload = function() {
            $('#harga').on('keyup', function() {
                var harga = $('#harga').val();
                var jumlah = $('#jumlah').val();

                var total = harga * jumlah;

                $("#total").val(total);


            });


            $('#jumlah').on('keyup', function() {
                var harga = $('#harga').val();
                var jumlah = $('#jumlah').val();

                var total = harga * jumlah;

                $("#total").val(total);
            });


        }

    </script>

@endsection

@endsection
