@extends('app.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>My Profile</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">My Profile</a></div>
                    <div class="breadcrumb-item"><a href="#">Forms</a></div>
                    <div class="breadcrumb-item">Editor</div>
                </div>
            </div>

            <div class="section-body">
                <h2 class="section-title">My Profile</h2>
                <form class="user" method="POST" action="{{ route('update_pass') }}">
                    @csrf
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="current_password">Current Password</label>
                                    <input id="current_password" type="password" class="form-control"
                                        name="current_password" autofocus>
                                </div>
                                <div class="form-group col-12">
                                    <label for="password">New Password</label>
                                    <input id="password" type="password" class="form-control" name="password" autofocus>
                                </div>
                                <div class="form-group col-12">
                                    <label for="password_confirmation">Confirm Password</label>
                                    <input id="password_confirmation" type="password" class="form-control"
                                        name="password_confirmation" autofocus>
                                </div>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    Update
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </section>
    </div>
    </div>

@endsection
