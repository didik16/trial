<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function internal_user()
    {

        $data = User::select('users.*', 'roles.name as nama_role')
            ->join('model_has_roles',  'users.id', '=', 'model_has_roles.model_id')
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->where('roles.name', '!=', 'user')
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('admin/user/internal_user', [
            'data' => $data
        ]);
    }

    public function public_user()
    {

        $data = User::select('users.*', 'roles.name as nama_role')
            ->join('model_has_roles',  'users.id', '=', 'model_has_roles.model_id')
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->where('roles.name', 'user')
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('admin/user/public_user', [
            'data' => $data
        ]);
    }

    public function add_user()
    {
        return view('admin/user/add_user');
    }

    public function store_add_user(Request $req)
    {
        $messages = [];

        $this->validate($req, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required', 'string', 'min:8',
        ], $messages);

        User::create([
            'name' => $req->name,
            'email' => $req->email,
            'password' => Hash::make($req->password),
        ])->assignRole($req->role);

        return redirect()->back();
    }
}
