<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Auth;
use App\Expense;


class ExpenseController extends Controller
{

    protected $request;


    public function index()
    {

        $data = Expense::orderBy('created_at', 'DESC')->get();

        return view('accounting/expense/index', [
            'data' => $data
        ]);
    }

    public function add()
    {

        return view('accounting/expense/add_expense');
    }

    public function store(Request $req)
    {

        $messages = [
            // 'total_harga.required' => 'Please Fill the Correct Data',
        ];

        $this->validate($req, [
            'nama_barang' => 'required',
            'harga_barang' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'photo' => 'required',
            'total' => 'required|numeric',
            'nama_suplier' => 'required',
            'alamat_suplier' => 'required',
        ], $messages);

        if ($req->hasFile('photo')) {
            $resorce = $req->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/image", $name);
        }

        Expense::create([
            'nama_barang' => $req->nama_barang,
            'harga_barang' =>  $req->harga_barang,
            'jumlah' => $req->jumlah,
            'gambar' => $name,
            'total' => $req->total,
            'nama_suplier' => $req->nama_suplier,
            'alamat_suplier' => $req->alamat_suplier,
        ]);

        return redirect()->back();
    }

    public function edit($id)
    {
        $data = Expense::where('id', $id)->firstOrFail();

        return view('accounting/expense/edit', ['data' => $data]);
    }


    public function update($id, Request $request)
    {
        $messages = [
            // 'total_harga.required' => 'Please Fill the Correct Data',
        ];

        $this->validate($request, [
            'nama_barang' => 'required',
            'harga_barang' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'total' => 'required|numeric',
            'nama_suplier' => 'required',
            'alamat_suplier' => 'required',
        ], $messages);

        if ($request->hasFile('photo')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/image", $name);

            $blog = Expense::find($id);
            $blog->nama_barang = $request->nama_barang;
            $blog->harga_barang = $request->harga_barang;
            $blog->jumlah = $request->jumlah;
            $blog->gambar = $request->name;
            $blog->total = $request->total;
            $blog->nama_suplier = $request->nama_suplier;
            $blog->alamat_suplier = $request->alamat_suplier;
            $blog->save();
        } else {
            $blog = Expense::find($id);
            $blog->nama_barang = $request->nama_barang;
            $blog->harga_barang = $request->harga_barang;
            $blog->jumlah = $request->jumlah;
            $blog->total = $request->total;
            $blog->nama_suplier = $request->nama_suplier;
            $blog->alamat_suplier = $request->alamat_suplier;
            $blog->save();
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        $delete = Expense::where('id', $id)->delete();

        return redirect()->back();
    }
}
