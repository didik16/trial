@extends('app.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>My Profile</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">My Profile</a></div>
                    <div class="breadcrumb-item"><a href="#">Forms</a></div>
                    <div class="breadcrumb-item">Editor</div>
                </div>
            </div>

            <a href="{{ route('update_profile') }}" class="btn btn-primary mb-3" style="color: white">Change Password</a>

            <div class="section-body">
                <h2 class="section-title">My Profile</h2>
                <form class="user" method="POST" action="{{ route('update_profile') }}">
                    @csrf
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-4">
                            @isset($data->detail_user->image)
                                <img src="{{ asset('image/' . $data->detail_user->image) }}" width="100%">
                            @else
                                <img src="https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png"
                                    width="100%">
                            @endisset

                            <input type="file" name="photo" class="dropify">
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="first_name">Name</label>
                                    <input id="first_name" type="text" class="form-control" name="name"
                                        value="{{ $data->name }}" autofocus>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control" name="email"
                                        value="{{ $data->email }}">
                                </div>
                                <div class="form-group col-6">
                                    <label for="phone">Phone Number</label>
                                    <input id="phone" type="text" class="form-control" name="phone"
                                        @isset($data->detail_user->phone)value="{{ $data->detail_user->phone }}"@endisset>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input id="address" type="address" class="form-control" name="address"
                                    @isset($data->detail_user->address)value="{{ $data->detail_user->address }}"@endisset>
                                <div class="invalid-feedback">
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    Update
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </section>
    </div>
    </div>

@endsection
