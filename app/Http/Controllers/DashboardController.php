<?php

namespace App\Http\Controllers;

use App\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Income;


class DashboardController extends Controller
{

    protected $request;


    public function index()
    {
        $user = User::count();

        $total_pendapatan = Income::sum('total');
        $total_pengeluaran = Expense::sum('total');
        $net = $total_pendapatan - $total_pengeluaran;


        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->format('m');
        $lastdateinmonth = Carbon::now()->format('t');

        $tanggal = [];
        $pendapatan = [];
        $pengeluaran = [];

        for ($i = 1; $i <= $lastdateinmonth; $i++) {
            $tanggal[] = $i;

            $income = DB::table('incomes')
                ->select(DB::raw('DATE(created_at) as tgl'), DB::raw('SUM(total) as total'))
                ->whereYear('created_at', $year)
                ->whereMonth('created_at', $month)
                ->whereDay('created_at', $i)
                ->groupBy('tgl')
                ->get();

            $expense = DB::table('expenses')
                ->select(DB::raw('DATE(created_at) as tgl'), DB::raw('SUM(total) as total'))
                ->whereYear('created_at', $year)
                ->whereMonth('created_at', $month)
                ->whereDay('created_at', $i)
                ->groupBy('tgl')
                ->get();


            $no = 0;
            $no1 = 0;
            foreach ($income as $income) {
                $no++;
            }

            foreach ($expense as $expense) {
                $no1++;
            }


            if ($no != 0) {
                $pendapatan[] = (int)$income->total;
            } else {
                $pendapatan[] = 0;
            }

            if ($no1 != 0) {
                $pengeluaran[] = (int)$expense->total;
            } else {
                $pengeluaran[] = 0;
            }
        }


        return view('admin/dashboard', [
            'user' => $user,
            'total_pendapatan' => $total_pendapatan,
            'total_pengeluaran' => $total_pengeluaran,
            'net' => $net,
            'tanggal' => $tanggal,
            'pendapatan' => $pendapatan,
            'pengeluaran' => $pengeluaran

        ]);
    }
}
